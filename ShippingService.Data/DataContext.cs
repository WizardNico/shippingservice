﻿using Microsoft.EntityFrameworkCore;
using ShippingService.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShippingService.Data
{
	public class DataContext : DbContext
	{
		public DbSet<PackageGroup> PackageGroups { get; set; }
		public DbSet<Package> Packages { get; set; }
		public DbSet<PackageRow> PackageRows { get; set; }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseSqlServer(@"Data Source=localhost;Initial Catalog=ShippingService;Persist Security Info=True;User ID=sa;Password=080809xx;Pooling=False;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=True");
		}
	}
}
