﻿using Microsoft.EntityFrameworkCore;
using ShippingService.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShippingService.Data.Repositories
{
	public static class PackageGroupRepository
	{
		public static async Task<PackageGroup> FindByIdAsync(this DbSet<PackageGroup> repository, long id)
		{
			PackageGroup group = await repository
				.Include(pkg => pkg.Packeges)
				.Where(grp => grp.Id == id)
				.SingleOrDefaultAsync();

			return (group);
		}
	}
}
