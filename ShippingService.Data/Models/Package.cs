﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShippingService.Data.Models
{
	public class Package
	{
		public long Id { get; set; }



		public List<PackageRow> PackegeRows { get; set; }


		
		public long PackageGroupId { get; set; }
		public PackageGroup PackageGroup { get; set; }
	}
}
