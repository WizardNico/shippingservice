﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ShippingService.Data.Models
{
	public class PackageGroup
	{
		public long Id { get; set; }



		[Timestamp]
		public byte[] RowVersion { get; set; }



		public DateTime UpdateTime { get; set; }

		public long OrderId { get; set; }

		public List<Package> Packeges { get; set; }
	}
}
