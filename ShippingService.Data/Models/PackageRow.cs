﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShippingService.Data.Models
{
	public class PackageRow
	{
		public long Id { get; set; }



		public long OrderRowId { get; set; }



		public long PackageId { get; set; }
		public Package Package { get; set; }
	}
}
