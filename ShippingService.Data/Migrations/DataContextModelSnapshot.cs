﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using ShippingService.Data;

namespace ShippingService.Data.Migrations
{
    [DbContext(typeof(DataContext))]
    partial class DataContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("ShippingService.Data.Models.Package", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("PackageGroupId");

                    b.HasKey("Id");

                    b.HasIndex("PackageGroupId");

                    b.ToTable("Packages");
                });

            modelBuilder.Entity("ShippingService.Data.Models.PackageGroup", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("OrderId");

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken()
                        .ValueGeneratedOnAddOrUpdate();

                    b.Property<DateTime>("UpdateTime");

                    b.HasKey("Id");

                    b.ToTable("PackageGroups");
                });

            modelBuilder.Entity("ShippingService.Data.Models.PackageRow", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("OrderRowId");

                    b.Property<long>("PackageId");

                    b.HasKey("Id");

                    b.HasIndex("PackageId");

                    b.ToTable("PackageRows");
                });

            modelBuilder.Entity("ShippingService.Data.Models.Package", b =>
                {
                    b.HasOne("ShippingService.Data.Models.PackageGroup", "PackageGroup")
                        .WithMany("Packeges")
                        .HasForeignKey("PackageGroupId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ShippingService.Data.Models.PackageRow", b =>
                {
                    b.HasOne("ShippingService.Data.Models.Package", "Package")
                        .WithMany("PackegeRows")
                        .HasForeignKey("PackageId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
