﻿using AgileMQ.Bus;
using AgileMQ.Enums;
using AgileMQ.Interfaces;
using ShippingService.Data;
using ShippingService.Loggers;
using ShippingService.Subscribers;
using System;

namespace ShippingService
{
	class Program
	{
		static void Main(string[] args)
		{
			using (IAgileBus bus = new RabbitMQBus("HostName=localhost;Port=5672;UserName=guest;Password=guest;Timeout=3;RetryLimit=9;PrefetchCount=100;AppId=ShippingService"))
			{
				bus.Logger = new ConsoleLogger();

				bus.Container.Register<DataContext, DataContext>(InjectionScope.Message);

				bus.Suscribe(new ShippingSubscriber());
				bus.Suscribe(new PackageGroupSubscriber());

				Console.ReadLine();
			}
		}
	}
}