﻿using AgileMQ.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using AgileMQ.Enums;

namespace ShippingService.Loggers
{
	public class ConsoleLogger : IAgileLogger
	{
		public void OnRpcRequest<T>(RpcMethod rpcMethod, string message, string correlationId, string appId)
		{
			Console.WriteLine(appId + " send a RPC request: " + message);
			Console.WriteLine();
		}

		public void OnRpcResponse<T>(RpcMethod rpcMethod, string message, string correlationId, string appId)
		{
			Console.WriteLine("Send a RPC response about " + rpcMethod + " method to " + appId + ": " + message);
			Console.WriteLine();
		}

		public void OnRpcError<T>(RpcMethod rpcMethod, string message, Exception exception, int retryIndex, int retryLimit, string appId)
		{
			if (retryIndex == retryLimit)
			{
				Console.WriteLine(appId + ": an error occurred, " + exception.Message);
				Console.WriteLine();
			}
		}

		public void OnNotify<T>(string eventMethod, string message, string appId)
		{
			Console.WriteLine(eventMethod + " method notified by " + appId);
			Console.WriteLine();
		}

		public void OnNotifyError<T>(string eventMethod, string message, Exception exception, int retryIndex, int retryLimit, string appId)
		{
			if (retryIndex == retryLimit)
			{
				Console.WriteLine(appId + ": an error occurred, on method: " + eventMethod + ": " + exception.Message);
				Console.WriteLine();
			}
		}
	}
}
