﻿using System;
using System.Collections.Generic;
using System.Text;
using DataModels = ShippingService.Data.Models;
using EcommercePackagesModels = ShippingService.Directories.Ecommerce.Shipping.Models;

namespace ShippingService.Utilities
{
	public class Mapper
	{
		public static EcommercePackagesModels.PackageGroup Map(DataModels.PackageGroup group)
		{
			return new EcommercePackagesModels.PackageGroup()
			{
				Id = group.Id,
				OrderId = group.OrderId,
			};
		}
	}
}
