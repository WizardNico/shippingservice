﻿using AgileMQ.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using AgileMQ.Containers;
using System.Threading.Tasks;
using ShippingService.Directories.Ecommerce.Shipping.Models;
using AgileMQ.DTO;
using ShippingService.Data;
using DataModels = ShippingService.Data.Models;
using ShippingService.Utilities;

namespace ShippingService.Subscribers
{
	public class PackageGroupSubscriber : IAgileSubscriber<PackageGroup>
	{
		public IAgileBus Bus { get; set; }

		public Task DeleteAsync(PackageGroup model, MessageContainer container)
		{
			throw new NotImplementedException();
		}

		public Task<PackageGroup> GetAsync(PackageGroup model, MessageContainer container)
		{
			throw new NotImplementedException();
		}

		public Task<List<PackageGroup>> GetListAsync(Dictionary<string, object> filter, MessageContainer container)
		{
			throw new NotImplementedException();
		}

		public Task<Page<PackageGroup>> GetPagedListAsync(Dictionary<string, object> filter, long pageIndex, int pageSize, MessageContainer container)
		{
			throw new NotImplementedException();
		}

		public async Task<PackageGroup> PostAsync(PackageGroup model, MessageContainer container)
		{
			DataContext ctx = container.Resolve<DataContext>();

			DataModels.PackageGroup group = new DataModels.PackageGroup()
			{
				OrderId = model.OrderId.Value,
				UpdateTime = DateTime.Now,
				Packeges = new List<DataModels.Package>(),
			};

			await ctx.PackageGroups.AddAsync(group);
			await ctx.SaveChangesAsync();

			model = Mapper.Map(group);

			return (model);
		}

		public Task PutAsync(PackageGroup model, MessageContainer container)
		{
			throw new NotImplementedException();
		}
	}
}
