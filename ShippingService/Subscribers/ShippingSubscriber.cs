﻿using AgileMQ.Interfaces;
using ShippingService.Directories.Ecommerce.Shipping.Models;
using System;
using System.Collections.Generic;
using DataModels = ShippingService.Data.Models;
using AgileMQ.Containers;
using AgileMQ.DTO;
using System.Threading.Tasks;
using ShippingService.Data;
using ShippingService.Directories.Ecommerce.Orders.Models;
using System.Linq;
using ShippingService.Data.Repositories;

namespace ShippingService.Subscribers
{
	public class ShippingSubscriber : IAgileSubscriber<Package>
	{
		public IAgileBus Bus { get; set; }

		public Task DeleteAsync(Package model, MessageContainer container)
		{
			throw new NotImplementedException();
		}

		public Task<Package> GetAsync(Package model, MessageContainer container)
		{
			throw new NotImplementedException();
		}

		public Task<List<Package>> GetListAsync(Dictionary<string, object> filter, MessageContainer container)
		{
			throw new NotImplementedException();
		}

		public Task<Page<Package>> GetPagedListAsync(Dictionary<string, object> filter, long pageIndex, int pageSize, MessageContainer container)
		{
			throw new NotImplementedException();
		}

		public Task<Package> PostAsync(Package model, MessageContainer container)
		{
			throw new NotImplementedException();
		}

		public Task PutAsync(Package model, MessageContainer container)
		{
			throw new NotImplementedException();
		}

		public async Task CreatedAsync(Package model, MessageContainer container)
		{
			DataContext ctx = container.Resolve<DataContext>();

			//prendo il gruppo relativo a questo package
			DataModels.PackageGroup group = await ctx.PackageGroups.FindByIdAsync(model.GroupId.Value);

			//Modifico la data di update
			group.UpdateTime = DateTime.Now;

			//Creo il pacchetto
			DataModels.Package newPackage = new DataModels.Package()
			{
				PackegeRows = new List<DataModels.PackageRow>(),
				PackageGroup = group,
			};

			foreach (long rowId in model.OrderRowsId)
			{
				//Creo la riga
				DataModels.PackageRow newPackageRow = new DataModels.PackageRow()
				{
					OrderRowId = rowId,
					Package = newPackage
				};

				//Aggiungo la riga al package
				newPackage.PackegeRows.Add(newPackageRow);
			}

			//Collego il pacchetto al gruppo relativo 
			group.Packeges.Add(newPackage);

			//Aggiungo il package al DB
			await ctx.Packages.AddAsync(newPackage);

			//Prendo la lista di orderRows relativa a questo ordine
			Dictionary<string, object> filter = new Dictionary<string, object>
			{
				{ "orderId", group.OrderId },
			};
			List<OrderRow> items = await Bus.GetListAsync<OrderRow>(filter);

			//prendo gli orderRow processati fino ad ora con l'id relativo all'ordine in questione
			var processedOrderRow = ctx.PackageRows.Where(pkr => pkr.Package.PackageGroup.OrderId == group.OrderId).Count();

			//Se ho finito di aggiungere le righe degli ordini allora notifico l'evento e cambio lo stato dell'ordine
			if (items.Count() == processedOrderRow + model.OrderRowsId.Count())
			{
				OrderHeader order = new OrderHeader
				{
					Id = group.OrderId,
				};
				await Bus.NotifyAsync(order, "Complete");
			}

			await ctx.SaveChangesAsync();
		}
	}
}
