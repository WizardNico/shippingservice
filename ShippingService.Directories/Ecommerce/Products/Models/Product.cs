﻿using AgileMQ.Attributes;
using ShippingService.Directories.Ecommerce.Products.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ShippingService.Directories.Ecommerce.Products.Models
{
	[QueuesConfig(Directory = "Ecommerce", Subdirectory = "Products", ResponseEnabled = true)]
	public class Product
	{
		[Required]
		[MinLength(3)]
		[MaxLength(10)]
		public string Code { get; set; }

		[Required]
		[MinLength(5)]
		[MaxLength(100)]
		public string Description { get; set; }

		[Required]
		[MinLength(3)]
		[MaxLength(10)]
		public string CategoryCode { get; set; }

		[Required]
		public double? Price { get; set; }

		[Required]
		public double? Vat { get; set; }

		public int? Availability { get; set; }

		public int? Movements { get; set; }

		public List<ProductOption> Options { get; set; }
	}
}
