﻿using AgileMQ.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ShippingService.Directories.Ecommerce.Shipping.Models
{
	[QueuesConfig(Directory = "Ecommerce", Subdirectory = "Shipping", ResponseEnabled = true)]
	public class Package
	{
		[Required]
		[MinLength(12)]
		[MaxLength(12)]
		public string Number { get; set; }

		[Required]
		public DateTime? ExpectedDeliveryDate { get; set; }

		public DateTime? ReallyDeliveryDate { get; set; }

		[Required]
		public long? GroupId { get; set; }

		[Required]
		public List<long> OrderRowsId { get; set; }
	}
}
